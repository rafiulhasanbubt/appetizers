//
//  Order.swift
//  Appetizers
//
//  Created by rafiul hasan on 14/10/21.
//

import Foundation

final class Order: ObservableObject {
    
    @Published var items: [Appetizer] = []
    
    var totalPrice: Double {
        items.reduce(0) { $0 + $1.price }
    }
    
    func add(_ appetizer: Appetizer) {
        items.append(appetizer)
    }
    
    func deleteItems(at offSet: IndexSet) {
        items.remove(atOffsets: offSet)
    }
}
