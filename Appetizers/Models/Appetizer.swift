//
//  Appetizer.swift
//  Appetizers
//
//  Created by rafiul hasan on 13/10/21.
//

import Foundation
import SwiftUI

struct Appetizer: Decodable, Identifiable {
    let id: Int
    let name: String
    let description: String
    let price: Double
    let imageURL: String
    let calories: Int
    let protein: Int
    let carbs: Int
}

struct AppetizerResponse: Decodable {
    let request: [Appetizer]
}

struct MockData {
    static let sampleAppetizer = Appetizer(id: 10001, name: "Test Appetizers", description: "Test Appetizers Test Appetizers Test Appetizers Test Appetizers Test Appetizers", price: 19.99, imageURL: "", calories: 99, protein: 99, carbs: 99)
    
    //MARK: order data
    static let orderItemOne = Appetizer(id: 1001, name: "order Item One", description: "Test Appetizers Test Appetizers Test Appetizers Test Appetizers Test Appetizers", price: 9.99, imageURL: "", calories: 59, protein: 80, carbs: 09)
    static let orderItemTwo = Appetizer(id: 1002, name: "order Item Two", description: "Test Appetizers Test Appetizers Test Appetizers Test Appetizers Test Appetizers", price: 7.99, imageURL: "", calories: 49, protein: 79, carbs: 89)
    static let orderItemThree = Appetizer(id: 1003, name: "order Item Three", description: "Test Appetizers Test Appetizers Test Appetizers Test Appetizers Test Appetizers", price: 11.99, imageURL: "", calories: 39, protein: 29, carbs: 19)
    static let orderItemFour = Appetizer(id: 1004, name: "order Item Four", description: "Test Appetizers Test Appetizers Test Appetizers Test Appetizers Test Appetizers", price: 13.99, imageURL: "", calories: 09, protein: 90, carbs: 48)
    
    static let orderItems = [orderItemOne, orderItemTwo, orderItemThree, orderItemFour]
}
