//
//  User.swift
//  Appetizers
//
//  Created by rafiul hasan on 14/10/21.
//

import Foundation
import SwiftUI

struct User: Codable {
   var firstName = ""
   var lastName = ""
   var email = ""
   var birthdate = Date()
   var extraNapkins = false
   var frequentRefills = false
}
