//
//  EmptyStateView.swift
//  Appetizers
//
//  Created by rafiul hasan on 14/10/21.
//

import SwiftUI

struct EmptyStateView: View {
    let imageName: String
    let message: String
    
    var body: some View {
        ZStack {
            Color(.systemBackground)
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                Image(imageName)
                    .resizable()
                    .scaledToFit()
                    .frame(height: 150)
                
                Text(message)
                    .font(.title3)
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.center)
                    .foregroundColor(.secondary)
                    .padding()
            }
        }
    }
}

struct EmptyStateView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyStateView(imageName: "\(Image(systemName: "photo"))", message: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.")
    }
}
