//
//  APButtonView.swift
//  Appetizers
//
//  Created by rafiul hasan on 13/10/21.
//

import SwiftUI

struct APButtonView: View {
    
    var title: LocalizedStringKey
    
    var body: some View {
        Text(title)
            .font(.title3)
            .fontWeight(.semibold)
            .frame(width: 260, height: 50)
            .foregroundColor(.white)
            .background(Color.red)
            .cornerRadius(10)
    }
}

struct APButtonView_Previews: PreviewProvider {
    static var previews: some View {
        APButtonView(title: "Title come here")
    }
}
