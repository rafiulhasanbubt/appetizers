//
//  NutritionInfoView.swift
//  Appetizers
//
//  Created by rafiul hasan on 13/10/21.
//

import SwiftUI

struct NutritionInfoView: View {
    let title: String
    let value: Int
    
    var body: some View {
        VStack(spacing: 5) {
            Text(title)
                .bold()
                .font(.caption)
            Text("\(value)")
                .foregroundColor(.secondary)
                .fontWeight(.semibold)
                .italic()
        }
    }
}

struct NutritionInfoView_Previews: PreviewProvider {
    static var previews: some View {
        NutritionInfoView(title: "nutrition name", value: 99)
    }
}
