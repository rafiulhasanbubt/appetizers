//
//  AccountView.swift
//  Appetizers
//
//  Created by rafiul hasan on 13/10/21.
//

import SwiftUI

struct AccountView: View {
    @StateObject var accountVM = AccountViewModel()
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Personal Info")) {
                    TextField("First Name", text: $accountVM.user.firstName)
                    TextField("Last Name", text: $accountVM.user.lastName)
                    TextField("Email", text: $accountVM.user.email)
                        .keyboardType(.emailAddress)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                    
                    DatePicker("Birthday", selection: $accountVM.user.birthdate, displayedComponents: .date)
                    
                    Button {
                        accountVM.saveChanges()
                    } label: {
                        Text("Save Changes")
                    }

                }
                
                Section(header: Text("Requests")) {
                    Toggle("Extra Napkins", isOn: $accountVM.user.extraNapkins)
                    Toggle("Frequent Refills", isOn: $accountVM.user.frequentRefills)
                }
                .toggleStyle(SwitchToggleStyle(tint: .red))
            }
            .navigationTitle("😀 Account")
        }
        .onAppear {
            accountVM.retrieveUser()
        }
        .alert(item: $accountVM.alertItem) { alertItem in
            Alert(title: alertItem.title, message: alertItem.message, dismissButton: alertItem.dismissButton)
        }
    }
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView()
    }
}
