//
//  AppetizerDetailView.swift
//  Appetizers
//
//  Created by rafiul hasan on 13/10/21.
//

import SwiftUI

struct AppetizerDetailView: View {
    @EnvironmentObject var order: Order
    let appetizer: Appetizer
    @Binding var isShowingDetailView: Bool
    
    var body: some View {
        VStack {
            AppetizerRemoteImage(urlString: appetizer.imageURL)
                .aspectRatio(contentMode: .fit)
                .frame(width: 300, height: 225)
            
            VStack {
                Text(appetizer.name)
                    .font(.title2)
                    .fontWeight(.semibold)
                Text(appetizer.description)
                    .multilineTextAlignment(.center)
                    .font(.body)
                    .padding()
                
                HStack(spacing: 40) {
                    NutritionInfoView(title: "Calories", value: appetizer.calories)
                    NutritionInfoView(title: "Proteins", value: appetizer.protein)
                    NutritionInfoView(title: "Carbs", value: appetizer.carbs)
                }
            }
            
            Spacer()
            
            Button {
                order.add(appetizer)
                isShowingDetailView = false
            } label: {
                APButtonView(title: "$\(appetizer.price, specifier: "%.2f") - Add to Order")
            }.padding(.bottom, 30)

        }
        .frame(width: 300, height: 525)
        .background(Color(.systemBackground))
        .cornerRadius(12)
        .shadow(radius: 40)
        .overlay(Button{
            isShowingDetailView = false
        } label: {
            XDismissButtonView()
        }, alignment: .topTrailing)
        
    }
}

struct AppetizerDetailView_Previews: PreviewProvider {
    static var previews: some View {
        AppetizerDetailView(appetizer: MockData.sampleAppetizer, isShowingDetailView: .constant(true))
    }
}
