//
//  APError.swift
//  Appetizers
//
//  Created by rafiul hasan on 13/10/21.
//

import Foundation

enum APError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case unableToComplete
}
