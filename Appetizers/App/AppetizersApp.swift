//
//  AppetizersApp.swift
//  Appetizers
//
//  Created by rafiul hasan on 13/10/21.
//

import SwiftUI

@main
struct AppetizersApp: App {
    
    var order = Order()
    
    var body: some Scene {
        WindowGroup {
            AppetizerTabView()
                .environmentObject(order)
        }
    }
}
